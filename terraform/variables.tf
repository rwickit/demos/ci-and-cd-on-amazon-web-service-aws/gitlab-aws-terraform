variable "region" {
  description = "AWS Region name"
  type        = string
  default     = "us-east-1"
}

# variable "ci_cd_role_arn" {
#   description = "Assumable role arn for accessing AWS resources"
#   type        = string
# }

# variable "ci_cd_sts_session_name" {
#   description = "Assumable role session name"
#   type        = string
# }

# variable "ci_cd_sts_session_duration" {
#   description = "Time allowed for session"
#   type        = string
# }

# variable "ci_cd_web_identity_token" {
#   description = "GitLab CI/CD web identity token"
#   type        = string
# }