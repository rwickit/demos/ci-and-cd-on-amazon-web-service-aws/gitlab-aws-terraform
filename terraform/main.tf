terraform {

  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
  }

  backend "s3" {
    bucket = "demo-acb-cicd-bucket-cft"
    key    = "dev/demo-state2"
    region = "us-east-2"
  }

}
